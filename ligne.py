from alone import Alone
from Couleur import Couleur

class Ligne(Alone):
    """Class Ligne(alone)

    Attributes
    ----------
    __x2
    __y2

    methods
    -------
    +getX2
    -setX2
    +getY2
    -setY2
    """

    def __init__(self, x: int, y: int, x2: int, y2: int, couleur: Couleur):
        """Constructeur de la class Ligne(alone)
        """
        super().__init__(x, y, couleur)
        self.__setX2(x2)
        self.__setY2(y2)

    def getX2(self) -> int:
        """getter sur l'attribut __x2
        :return:
        """
        return self.__x2

    def __setX2(self, value: int) -> None:
        """setter sur l'attribut __x2 il permet sa valorisation
        :param value: int
        """
        self.__x2 = value

    def getY2(self) -> int:
        """getter sur l'attribut __y2
        :return:
        """
        return self.__y2

    def __setY2(self, value: int) -> None:
        """setter sur l'attribut __y2 il permet sa valorisation
        :param value: int
        """
        self.__y2 = value

    def __str__(self) -> str:
        """retourne un str à afficher lors d'un print d'un object
        """
        return ""