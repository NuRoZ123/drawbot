import sys

import keyboard
from PySide6.QtWidgets import QApplication, QMainWindow
from pynput.mouse import Controller, Button

from design.code.ui_mainWindow import Ui_MainPage
from typing import List
from PIL import Image
from math import sqrt
from configparser import ConfigParser
from Couleur import Couleur
import pyautogui
pyautogui.PAUSE = 0.0001
from settingWindow import WindowFinPartie
from alone import Alone
from ligne import Ligne
#pour le momen l'image est chat.jpg only


class MainWindows(QMainWindow):
    def __init__(self):
        # appel au constructeur de la classe Parent, ici QMainWindow
        super(MainWindows, self).__init__()
        # instanciation d'une interface à partir de la classe auto-générée depuis le designer.
        self.ui = Ui_MainPage()
        self.ui.setupUi(self)
        self.ui.logoQLabel.setPixmap("logo.png")
        self.ui.startQPushButton.clicked.connect(self.start)
        self.ui.settingsQPushButton.clicked.connect(self.setting)
        self.couleur = []
        jeux = ["garticPhone", "paint"]
        for i in range(0, len(jeux)):
            self.ui.comboBox.addItem(jeux[i])
        self.ui.CloseDuringQcheckBox.setChecked(True)
        self.settingWindow = WindowFinPartie()
        self.settingWindow.uiFinPartie.safeZoneQPushButton.clicked.connect(self.safeZone)
        self.settingWindow.hide()

    def getImageInPixel(self, lien: str) -> List:
        """recupere l'image et la renvoie en pixel
        :param lien:
        :return:
        """
        img = Image.open(lien)
        taille = (400, 400)
        img = img.resize(taille)
        self.imageSize = img.size
        return list(img.getdata())

    def transformViaDistance(self, image: List[tuple]) -> List[Couleur]:
        """retourne l'image via la pallette disponible
        :param self, image:
        :return:
        """
        image = list(image)
        for i in range(0, len(image)):
            distance = []
            for j in range(0, len(self.couleur)):
                distBetween = sqrt((self.couleur[j].getR() - image[i][0]) ** 2 + (self.couleur[j].getG() - image[i][1]) ** 2 + (self.couleur[j].getB() - image[i][2]) ** 2)
                distance.append(distBetween)

            mini = 30500
            indexMin = 30500
            for j in range(0, len(distance)):
                if distance[j] < mini:
                    mini = distance[j]
                    indexMin = j

            image[i] = self.couleur[indexMin]
        return image

    def getLineAndAlone(self, image: List[Couleur]):
        newImage = []
        ligneAlone = []
        for y in range(0, 400):
            for x in range(0, 399):
                if x == 0:
                    if image[400 * y + x] != image[400 * y + (x + 1)]:
                        if len(ligneAlone) >= 2:
                            newImage.append(Ligne(ligneAlone[0].getX(), ligneAlone[0].getY(), ligneAlone[len(ligneAlone) - 1].getY(), ligneAlone[len(ligneAlone) - 1].getY(), ligneAlone[0].getCouleur()))
                            ligneAlone = []
                        newImage.append(Alone(x, y, image[400 * y + x]))
                    else:
                        ligneAlone.append(Alone(x, y , image[400 * y + x]))

                if x == 398:
                    if image[400 * y + (x - 1)] != image[400 * y + x] and image[400 * y + (x + 1)] != image[400 * y + x]:
                        newImage.append(Alone(x, y, image[400 * y + x]))
                    else:
                        ligneAlone.append(Alone(x, y, image[400 * y + x]))

                    if image[400 * y + (x+1)] != image[400 * y + x]:
                        newImage.append(Alone(x + 1, y, image[400 * y + (x+1)]))
                    else:
                        ligneAlone.append(Alone(x, y, image[400 * y + (x + 1)]))

                else:
                    if image[400 * y + (x-1)] != image[400 * y + x] and image[400 * y + (x+1)] != image[400 * y + x]:
                        newImage.append(Alone(x, y, image[400 * y + x]))
                    else:
                        ligneAlone.append(Alone(x, y , image[400 * y + x]))

    def drawLine(self, x: int, y: int, x2: int, y2: int):
        mouse = Controller()
        pyautogui.moveTo(x, y)
        mouse.press(Button.left)
        pyautogui.moveTo(x2, y2)
        mouse.release(Button.left)

    def start(self) -> None:
        config = ConfigParser()
        config.read('settings.ini')

        self.couleur = config.options(self.ui.comboBox.itemText(self.ui.comboBox.currentIndex()))
        for i in range(0, len(self.couleur)):
            configColor = config.get(self.ui.comboBox.itemText(self.ui.comboBox.currentIndex()), self.couleur[i]).split()
            self.couleur[i] = Couleur(self.couleur[i], int(configColor[0]), int(configColor[1]), int(configColor[2]), int(configColor[3]), int(configColor[4]))

        image = self.transformViaDistance(self.getImageInPixel("mcdo.png"))

        # for uneCouleur in self.couleur:
        #     pyautogui.moveTo(uneCouleur.getPosX(), uneCouleur.getPosY())
        #     pyautogui.click(button='left', clicks=2, interval=0.2)
        #     for y in range(0, 400):
        #         for x in range(0, 400):
        #
        #             if keyboard.is_pressed("esc"): break
        #         if keyboard.is_pressed("esc"): break
        #     if keyboard.is_pressed("esc"): break

        for uneCouleur in self.couleur:
            pyautogui.moveTo(uneCouleur.getPosX(), uneCouleur.getPosY())
            pyautogui.click(button='left', clicks=2, interval=0.2)
            for y in range(0, 400):
                for x in range(0, 400):
                    if x % 3 == 0 and y % 3 == 0:
                        if image[400 * y + x] == uneCouleur:
                            pyautogui.moveTo(x + 700, y + 400)
                            pyautogui.click()
                    if keyboard.is_pressed("esc"): break
                if keyboard.is_pressed("esc"): break
            if keyboard.is_pressed("esc"): break

    def setting(self,) -> None:
        """donne accés au paramétre
        :param self,:
        :return:
        """
        self.hide()
        self.settingWindow.show()

    def safeZone(self) -> None:
        print("se")





if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindows()
    window.show()

    sys.exit(app.exec_())
