from PySide6.QtWidgets import QApplication, QMainWindow
from design.code.ui_settingWindow import Ui_settingWindow

class WindowFinPartie(QMainWindow):
      def __init__(self):
        super(WindowFinPartie, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.uiFinPartie = Ui_settingWindow()
        self.uiFinPartie.setupUi(self)
