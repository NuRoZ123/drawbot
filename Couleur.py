class Couleur:
    """Class Couleur

    Attributes
    ----------
    nom: nom de la couleur
    posX: position X de la couleur
    posY: position Y de la couleur
    r: couleur entre 0 et 255 pour le rouge
    g: couleur entre 0 et 255 pour le vert
    b: couleur entre 0 et 255 pour le bleu

    methods
    -------
    +__init__(str, int, int, int, int, int)
    +getNom(): str
    -setNom(str)
    +getPosX(): int
    -setPosX(int)
    +getPosY(): int
    -setPosY(int)
    +getR(): int
    -setR(int)
    +getG(): int
    -setG(int)
    +getB(): int
    -setB(int)
    +__str__(): str
    """

    def __init__(self, nom: str, posX: int, posY: int, r: int, g: int, b: int):
        """Constructeur de la class Couleur
        """
        self.__setNom(nom)
        self.__setPosX(posX)
        self.__setPosY(posY)
        self.__setR(r)
        self.__setG(g)
        self.__setB(b)

    def getNom(self) -> str:
        """getter sur l'attribut __nom
        :return:
        """
        return self.__nom

    def __setNom(self, value: str) -> None:
        """setter sur l'attribut __nom il permet sa valorisation
        :param value: str:
        """
        self.__nom = value

    def getPosX(self) -> int:
        """getter sur l'attribut __posX
        :return:
        """
        return self.__posX

    def __setPosX(self, value: int) -> None:
        """setter sur l'attribut __posX il permet sa valorisation
        :param value: int:
        """
        self.__posX = value

    def getPosY(self) -> int:
        """getter sur l'attribut __posY
        :return:
        """
        return self.__posY

    def __setPosY(self, value: int) -> None:
        """setter sur l'attribut __posY il permet sa valorisation
        :param value: int:
        """
        self.__posY = value

    def getR(self) -> int:
        """getter sur l'attribut __r
        :return:
        """
        return self.__r

    def __setR(self, value: int) -> None:
        """setter sur l'attribut __r il permet sa valorisation
        :param value: int:
        """
        self.__r = value

    def getG(self) -> int:
        """getter sur l'attribut __g
        :return:
        """
        return self.__g

    def __setG(self, value: int) -> None:
        """setter sur l'attribut __g il permet sa valorisation
        :param value: int:
        """
        self.__g = value

    def getB(self) -> int:
        """getter sur l'attribut __b
        :return:
        """
        return self.__b

    def __setB(self, value: int) -> None:
        """setter sur l'attribut __b il permet sa valorisation
        :param value: int:
        """
        self.__b = value

    def __str__(self) -> str:
        """retourne un str à afficher lors d'un print d'un object
        """
        return self.__nom