from configparser import ConfigParser
from PIL import Image
from typing import List
from time import sleep
from pyautogui import moveTo as setSouris, dragRel as moveSouris, click
from Couleur import Couleur
from math import sqrt

def getPixelImage(lien: str) -> List[tuple]:
    """permet de retourner un liste de pixel pour l'image mis en paramètre
    :param lien:
    :return:
    """
    img = Image.open(lien)
    #img = img.resize((int(img.size[0]/3), int(img.size[1]/3)))
    return list(img.getdata())

def transformImage(image: List[tuple]) -> List[tuple]:
    """transforme une image mit en parametre avec la palette de couleur
    :param image:
    :return:
    """
    image = list(image)
    for i in range(0, len(image)):
        distance = []
        for j in range(0, len(couleur)):
            distBetween = sqrt((couleur[j].getR() - image[i][0]) ** 2 + (couleur[j].getG() - image[i][1]) ** 2 + (couleur[j].getB() - image[i][2]) ** 2)
            distance.append(distBetween)

        mini = 30500
        indexMin = 30500
        for j in range(0, len(distance)):
            if distance[j] < mini:
                mini = distance[j]
                indexMin = j

        image[i] = couleur[indexMin]
    return image

if __name__ == '__main__':
    config = ConfigParser()
    config.read('settings.ini')

    gartic = False
    paint = True

    if gartic:
        couleur= ["black", "white", "darkGreen", "green", "darkYellow", "yellow", "darkGray", "gray", "darkRed", "red", "darkPink", "pink", "darkBlue", "blue", "darkOrange", "orange", "darkBeige", "beige"]
        for i in range(0, len(couleur)):
            paramCouleur = config.get("garticPhone", couleur[i])
            paramCouleur = paramCouleur.split()
            couleur[i] = Couleur(couleur[i], int(paramCouleur[0]), int(paramCouleur[1]), int(paramCouleur[2]),int(paramCouleur[3]), int(paramCouleur[4]))

    elif paint:
        couleur = ["black", "white", "darkGray", "gray", "darkRed", "brown", "red", "pink", "orange", "gold", "yellow", "lightYellow", "green", "lightGreen", "blue", "lightBlue", "bluePurple", "blueGray", "purple", "lightPurple"]
        for i in range(0, len(couleur)):
            paramCouleur = config.get("paint", couleur[i])
            paramCouleur = paramCouleur.split()
            couleur[i] = Couleur(couleur[i], int(paramCouleur[0]), int(paramCouleur[1]), int(paramCouleur[2]),
                                 int(paramCouleur[3]), int(paramCouleur[4]))
    else:
        couleur = []

    image = transformImage(getPixelImage("logo.png"))

    count = 0
    oldColor = (0, 0, 0)
    for uneCouleur in couleur:
        setSouris(uneCouleur.getPosX(), uneCouleur.getPosY())
        click()
        if uneCouleur.getR() == 255 and uneCouleur.getG() == 255 and uneCouleur.getB() == 255:
            pass
        else:
            for i in range(0, 400, 5):
                for j in range(0, 400, 5):
                    if image[j*400+i] == uneCouleur:
                        setSouris(600 + i, 400 + j)
                        click()




