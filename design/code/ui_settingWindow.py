# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settingWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_settingWindow(object):
    def setupUi(self, settingWindow):
        if not settingWindow.objectName():
            settingWindow.setObjectName(u"settingWindow")
        settingWindow.resize(559, 476)
        self.comboBox_2 = QComboBox(settingWindow)
        self.comboBox_2.setObjectName(u"comboBox_2")
        self.comboBox_2.setGeometry(QRect(30, 30, 499, 22))
        self.safeZoneQPushButton = QPushButton(settingWindow)
        self.safeZoneQPushButton.setObjectName(u"safeZoneQPushButton")
        self.safeZoneQPushButton.setGeometry(QRect(80, 390, 75, 24))
        self.quitterQPushButton = QPushButton(settingWindow)
        self.quitterQPushButton.setObjectName(u"quitterQPushButton")
        self.quitterQPushButton.setGeometry(QRect(410, 390, 75, 24))

        self.retranslateUi(settingWindow)
        self.quitterQPushButton.clicked.connect(settingWindow.close)

        QMetaObject.connectSlotsByName(settingWindow)
    # setupUi

    def retranslateUi(self, settingWindow):
        settingWindow.setWindowTitle(QCoreApplication.translate("settingWindow", u"NuRoZ setting draw bot", None))
        self.safeZoneQPushButton.setText(QCoreApplication.translate("settingWindow", u"safe zone", None))
        self.quitterQPushButton.setText(QCoreApplication.translate("settingWindow", u"quitter !", None))
    # retranslateUi

