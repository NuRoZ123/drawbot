# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_MainPage(object):
    def setupUi(self, MainPage):
        if not MainPage.objectName():
            MainPage.setObjectName(u"MainPage")
        MainPage.resize(559, 476)
        self.CloseDuringQcheckBox = QCheckBox(MainPage)
        self.CloseDuringQcheckBox.setObjectName(u"CloseDuringQcheckBox")
        self.CloseDuringQcheckBox.setGeometry(QRect(320, 10, 231, 20))
        self.layoutWidget = QWidget(MainPage)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(30, 30, 501, 431))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.titreQlabel = QLabel(self.layoutWidget)
        self.titreQlabel.setObjectName(u"titreQlabel")
        font = QFont()
        font.setFamily(u"Times New Roman")
        font.setPointSize(20)
        self.titreQlabel.setFont(font)
        self.titreQlabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.titreQlabel)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_3)

        self.logoQLabel = QLabel(self.layoutWidget)
        self.logoQLabel.setObjectName(u"logoQLabel")
        self.logoQLabel.setPixmap(QPixmap(u"../../../../Downloads/logoNotEnd 128x128.png"))
        self.logoQLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.logoQLabel)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_4)

        self.messageQLabel = QLabel(self.layoutWidget)
        self.messageQLabel.setObjectName(u"messageQLabel")
        self.messageQLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.messageQLabel)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.comboBox = QComboBox(self.layoutWidget)
        self.comboBox.setObjectName(u"comboBox")

        self.verticalLayout.addWidget(self.comboBox)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.startQPushButton = QPushButton(self.layoutWidget)
        self.startQPushButton.setObjectName(u"startQPushButton")

        self.horizontalLayout.addWidget(self.startQPushButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.settingsQPushButton = QPushButton(self.layoutWidget)
        self.settingsQPushButton.setObjectName(u"settingsQPushButton")

        self.horizontalLayout.addWidget(self.settingsQPushButton)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.quitterQPushButton = QPushButton(self.layoutWidget)
        self.quitterQPushButton.setObjectName(u"quitterQPushButton")

        self.horizontalLayout.addWidget(self.quitterQPushButton)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(MainPage)
        self.quitterQPushButton.clicked.connect(MainPage.close)

        QMetaObject.connectSlotsByName(MainPage)
    # setupUi

    def retranslateUi(self, MainPage):
        MainPage.setWindowTitle(QCoreApplication.translate("MainPage", u"NuRoZ draw bot", None))
        self.CloseDuringQcheckBox.setText(QCoreApplication.translate("MainPage", u"fermer cette fenetre lors de l'execution", None))
        self.titreQlabel.setText(QCoreApplication.translate("MainPage", u"Bienvenue pr\u00eat \u00e0 dessiner? \n"
" (enfin presque)", None))
        self.logoQLabel.setText("")
        self.messageQLabel.setText("")
        self.startQPushButton.setText(QCoreApplication.translate("MainPage", u"d\u00e9marrer !", None))
        self.settingsQPushButton.setText(QCoreApplication.translate("MainPage", u"param\u00e8tre !", None))
        self.quitterQPushButton.setText(QCoreApplication.translate("MainPage", u"quitter !", None))
    # retranslateUi

