from Couleur import Couleur

class Alone:
    """Class Alone

    Attributes
    ----------
    __x
    __y
    __couleur

    methods
    -------
    +getX
    -setX
    +getY
    -setY
    +getCouleur
    -getCouleur
    """

    def __init__(self, x: int, y: int, couleur: Couleur):
        """Constructeur de la class Alone
        """
        self.__setX(x)
        self.__setY(y)
        self.__setCouleur(couleur)

    def getX(self) -> int:
        """getter sur l'attribut __x
        :return:
        """
        return self.__x

    def __setX(self, value: int) -> None:
        """setter sur l'attribut __x il permet sa valorisation
        :param value: int
        """
        self.__x = value

    def getY(self) -> int:
        """getter sur l'attribut __x
        :return:
        """
        return self.__x

    def __setY(self, value: int) -> None:
        """setter sur l'attribut __x il permet sa valorisation
        :param value: int
        """
        self.__x = value

    def getCouleur(self) -> Couleur:
        """getter sur l'attribut __couleur
        :return:
        """
        return self.__couleur

    def __setCouleur(self, value: Couleur) -> None:
        """setter sur l'attribut __couleur il permet sa valorisation
        :param value: Couleur
        """
        self.__couleur = value






    def __str__(self) -> str:
        """retourne un str à afficher lors d'un print d'un object
        """
        return ""